import React,{useState,useEffect} from 'react'
import "./style1.css"
export default function CountNumbers() {
    const[i,setI]=useState(0)
    const[ischecked,setischecked]=useState(false)
   

    useEffect(()=>{
      console.log(i);
    })


    const increment=()=>{
        if(ischecked){
            setI(i+5)
        }else{
            setI(i+1)
        }
    }
    const decrement=()=>{
        if(i>0){
            if(ischecked){
                setI(Math.max(0, i - 5));
              
               }  
            else{
                setI(i-1)
            }
        
            
        }
    }
    const handleevent=(e)=>{
      setischecked(e.target.checked)
     
    }
    const reset=()=>{
        setI(0)
    }

  return (
    <div className='main'>
      <div><h1>{i}</h1></div>
      <button onClick={increment}>increment</button>
      <button onClick={decrement}>decrement</button>
      <button onClick={reset}>reset</button>
      <div><input type='checkbox' onChange={handleevent}></input></div>
      
    </div>
  )
}
